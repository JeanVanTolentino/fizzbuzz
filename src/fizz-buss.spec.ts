import { FizzBuzz } from "./fizz-buzz";

describe('FizzBuzz', ()=>{
    describe('fizzBuzz', ()=>{
        it('should return appropriate values', ()=>{
            let fizzBuzz = new FizzBuzz()
            let value = fizzBuzz.fizzBuzz()
            expect(value).toContain('1\n2\nFizz\n4\nBuzz\nFizz\n7\n8\nFizz\nBuzz\n11\nFizz\n13\n14\nFizzBuzz')
        })
    })
})