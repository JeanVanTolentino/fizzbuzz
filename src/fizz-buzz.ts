export class FizzBuzz{
    fizzBuzz(){
        let answer = ''
        for (let i=1;100>=i;i++){
            if(i%3===0 && i%5===0){
                answer+="FizzBuzz"
            } else if (i%3===0){
                answer+="Fizz"
            } else if (i%5===0){
                answer +="Buzz"
            } else {
                answer+=`${i}`
            }

            if(i!=100){
                answer+='\n'
            }
        }
        return answer
    }
}